#![recursion_limit="128"]

extern crate proc_macro;
extern crate syn;
#[macro_use]
extern crate quote;

use proc_macro::TokenStream;
use syn::{Type, Data, Fields, DeriveInput, Meta, NestedMeta, Ident, Lit};
use syn::{Attribute};

extern crate serde_json;

#[proc_macro_derive(Schemize, attributes(JustTypeParam, schema_note))]
pub fn derive_schema(input: TokenStream) -> TokenStream {
    // Turn input into a syntax tree
    let ast = syn::parse(input).unwrap();

    // Build the impl
    impl_schema(&ast)
}

fn impl_schema(ast: &DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let mut followup_fields = Vec::new();
    let mut followup_types = Vec::new();
    let (note_fields, note_literals) = get_notes(&ast.attrs);
    let mut followup_notes_fields = Vec::new();
    let mut followup_notes_literals = Vec::new();
    // check attrs to see if special type?
    let mut special = false;
    for attr in &ast.attrs {
        if attr.path.is_ident("JustTypeParam") {
            special = true;
        }
    }
    if special {
        println!("Picking special type parameter!");
        // Pick our type param:
        for param in ast.generics.type_params() {
            println!("Param: {}", quote!(#param).to_string());
        }
    }
    match &ast.data {
        Data::Struct(s) => {
            if let Fields::Named(fields) = &s.fields {
                for f in &fields.named {
                    let mut typevar = None;
                    if let Type::Path(p) = &f.ty {
                        typevar = Some(p);
                    }
                    if let Some(id) = &f.ident {
                        followup_types.push(typevar.unwrap());
                        followup_fields.push(id);
                        let (notes, literals) = get_notes(&f.attrs);
                        followup_notes_fields.push(notes);
                        followup_notes_literals.push(literals);
                    }
                }
            }
        },
        syn::Data::Enum(_e) => {
            println!("Got an enum");
            panic!("Enums are not supported at this time.");
        },
        _ => {
            println!("Got something else");
            panic!("Whatever that is is not supported at this time.");
        },
    }
    let followup_fields_c = followup_fields.clone();
    let gen = quote! {
        impl Schemize for #name {
            fn get_schema() -> Value {
                let mut map = Map::new();
                #(
                    let childschema = <#followup_types>::get_schema();
                    let mut eventualschema;
                    if let Value::Object(ref childmap) = childschema {
                        let mut mchildmap = childmap.clone();
                        #(
                            mchildmap.insert(stringify!(#followup_notes_fields).to_string(),
                            json!(#followup_notes_literals));
                        )*
                        eventualschema = json!(mchildmap);
                    } else {
                        println!("Cannot put notes on a primitive type child in schema!");
                        eventualschema = childschema.clone();
                    }
                    map.insert(stringify!(#followup_fields).to_string(), eventualschema);
                    )*
                #(
                    map.insert(stringify!(#note_fields).to_string(), json!(#note_literals));
                    )*
                json!({
                    stringify!(#name) : map,
                })
            }
            fn register(&self, unto: &mut Server) {
                #(
                    self.#followup_fields_c.register(unto);
                )*
            }
        }
    };
    gen.into()
}

fn get_notes(attrs: &Vec<Attribute> ) -> (Vec<Ident>, Vec<Lit>) {
    let mut note_fields = Vec::new();
    let mut note_literals = Vec::new();
    for attr in attrs {
        if attr.path.is_ident("schema_note") {
            if let Some(Meta::List(metas)) = attr.interpret_meta() {
                for meta in &metas.nested {
                    if let NestedMeta::Meta(Meta::NameValue(pair)) = meta {
                        let lit = &pair.lit;
                        note_fields.push(pair.ident.clone());
                        note_literals.push(lit.clone());
                    } else {
                        panic!("Use like \
                               #[schema_note(humanname=\"Release The Hounds\")]");
                    }
                }
            } else {
                panic!("Use like \
                       #[schema_note(humanname=\"Release The Hounds\")]");
            }
        }
    }
    (note_fields, note_literals)
}

#[proc_macro_derive(FlowType)]
pub fn derive_string_name(input: TokenStream) -> TokenStream {
    // Turn input into a syntax tree
    let ast = syn::parse(input).unwrap();

    // Build the impl
    impl_string_name(&ast)
}

fn impl_string_name(ast: &DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl FlowType for #name {
            fn string_name() -> String {
                stringify!(#name).to_string()
            }
        }
    };
    gen.into()
}
