extern crate ws;
extern crate gotham;
extern crate serde_json;
extern crate serde;
extern crate url;
extern crate reqwest;
extern crate regex;
#[macro_use] extern crate schema_derive;

use ws::{listen, Handler, Result, Message, CloseCode, Handshake, Error};

use std::collections::{HashMap, HashSet};
use std::collections::hash_map::DefaultHasher;
use std::marker::PhantomData;
use std::hash::Hasher;

use std::sync::Arc;
use std::thread;
use std::thread::JoinHandle;
use std::process::Command;
use std::sync::mpsc::{channel, Sender, Receiver};

use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use serde_json::map::Map;

use gotham::handler::assets::FileOptions;
use gotham::router::builder::{build_simple_router, DefineSingleRoute, DrawRoutes};

use reqwest::Client;

use regex::Regex;

type PortId = u32;
type SocketIndex = HashMap<PortId, Sender<String>>;

trait Schemize {
    fn get_schema() -> Value where Self:Sized;
    fn register(&self, _unto: &mut Server) {}
}

/* CODE FROM http://virkkunen.net/ (Lumpio- on IRC)
 * thank you so much
 **/
macro_rules! wrapper_enum {
    (($trait:ty, $name:ident) { $($type:ident),* }) => {
        #[derive(Debug, Serialize, Deserialize)]
        enum $name {
            $(
                $type($type)
            ),*
        }

        impl $name {
            fn get_schemas() -> Vec<Value> {
                let mut ret = Vec::new();
                $(
                    ret.push(json!({"SCHEMA" : $type::get_schema() }));
                )*
                ret
            }
        }
        
        $(
            impl From<$type> for $name {
                fn from(value: $type) -> Self {
                    $name::$type(value)
                }
            }
        )*
        
        impl std::ops::Deref for $name {
            type Target = $trait;
            
            fn deref(&self) -> &Self::Target {
                match self {
                    $(
                        $name::$type(value) => value
                    ),*
                }
            }
        }
        impl std::ops::DerefMut for $name {
            fn deref_mut(&mut self) -> &mut Self::Target {
                match self {
                    $(
                        $name::$type(value) => value
                    ),*
                }
            }
        }
    }
}


struct Server {
    out: Arc<ws::Sender>,
    socket_index: SocketIndex, // string holds path
    node_vec: Vec<NodeWrapper>,
}

#[derive(Debug, Serialize, Deserialize, FlowType)]
struct Url(Option<String>);
#[derive(Debug, Serialize, Deserialize, FlowType)]
struct Html(Option<String>);
#[derive(Debug, Serialize, Deserialize, FlowType)]
struct Pdf(Option<String>);
#[derive(Debug, Serialize, Deserialize, FlowType)]
struct Str(Option<String>);
#[derive(Debug, Serialize, Deserialize, FlowType)]
struct Button();
#[derive(Debug, Serialize, Deserialize, FlowType)]
struct Bool(Option<bool>);

trait FlowType: std::fmt::Debug {
    // The following keeps string_name from being callable without picking
    // a specific struct. Which makes sense. It keeps out it out of the vtable;
    // where Self:Sized is sort of an inverse to C++ virtual.
    fn string_name() -> String where Self:Sized;
}

/*
wrapper_enum!{
    (FlowType, FlowTypeWrapper) {Url, Pdf, Str, Button}
}
*/

#[derive(Debug, Serialize, Deserialize)]
#[serde(default)]
struct InSocket<T: FlowType> {
    #[serde(skip)]
    piperx: Option<Receiver<String>>, 
    #[serde(skip)]
    pipetx: Sender<String>, 
    id: PortId,
    #[serde(skip)]
    _spooky: PhantomData<T>,
}
impl<T: FlowType> Schemize for InSocket<T> {
    fn get_schema() -> Value {
        let mut map = Map::new();
        map.insert("t".to_string(), json!(T::string_name()));
        json!({
            "InSocket" : map
        })
    }
    // create the FIFOs and put them in the map
    fn register(&self, unto: &mut Server) {
        println!("Registering! {}", self.id);
        unto.socket_index.insert(self.id, self.pipetx.clone());
    }
}
impl <T: FlowType> Default for InSocket<T> {
    fn default() -> Self {
        let (tx, rx) = channel();
        InSocket {
            piperx: Some(rx),
            pipetx: tx,
            id: 0,
            _spooky: PhantomData,
        }
    }
}

#[derive(Debug,Serialize, Deserialize)]
struct OutSocket<T: FlowType> {
    to: Option<PortId>,
    #[serde(skip)]
    _spooky: PhantomData<T>,
}

impl<T: FlowType> Schemize for OutSocket<T> {
    fn get_schema() -> Value {
        let mut map = Map::new();
        map.insert("t".to_string(), json!(T::string_name()));
        json!({
            "OutSocket" : map
        })
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct UserConstant<T: FlowType> {
    t: T,
}
impl<T: FlowType> Schemize for UserConstant<T> {
    fn get_schema() -> Value {
        let mut map = Map::new();
        map.insert("t".to_string(), json!(T::string_name()));
        json!({
            "UserConstant" : map
        })
    }
}

trait Node : Schemize {
    fn run_script(&mut self, ctx: &SocketIndex, out: &Arc<ws::Sender>)
        -> Option<JoinHandle<ServerMessage>>;
}

#[derive(Debug, Serialize, Deserialize, Schemize)]
#[schema_note(humanname="Manual URL Input")]
struct UrlInput {
    #[schema_note(humanname="Enter URL Here")]
    strent: UserConstant<Str>, 
    #[schema_note(humanname="That URL")]
    strout: OutSocket<Url>,
}

#[derive(Debug, Serialize, Deserialize, Schemize)]
#[schema_note(humanname="Render URL to PDF")]
struct PdfRender {
    #[schema_note(humanname="URL to render")]
    urlin: InSocket<Url>,
    #[schema_note(humanname="Resulting PDF")]
    pdfout: OutSocket<Pdf>,
}

#[derive(Debug, Serialize, Deserialize, Schemize)]
#[schema_note(humanname="Download a PDF")]
struct PdfDownload {
    #[schema_note(humanname="PDF to download via browser")]
    pdfin: InSocket<Pdf>,
}

#[derive(Debug, Serialize, Deserialize, Schemize)]
#[schema_note(humanname="Start")]
#[schema_note(postFabricate="runInit")]
struct Starter {
    #[schema_note(humanname="Start Processing Run")]
    start_button: UserConstant<Button>,
}

#[derive(Debug, Serialize, Deserialize, Schemize)]
#[schema_note(humanname="Web Scraper")]
struct Scraper {
    #[schema_note(humanname="URL to download")]
    urlin: InSocket<Url>,
    #[schema_note(humanname="Resulting HTML")]
    pageout: OutSocket<Html>,
}

#[derive(Debug, Serialize, Deserialize, Schemize)]
#[schema_note(humanname="HTML URL attribute extractor")]
struct AttributeExtractor {
    #[schema_note(humanname="Tag name (e.g. `a`)")]
    tag: UserConstant<Str>, 
    #[schema_note(humanname="Attribute (e.g. `href`)")]
    attribute: UserConstant<Str>, 
    #[schema_note(humanname="External links? (DANGEROUS)")]
    external: UserConstant<Bool>, 
    #[schema_note(humanname="HTML page")]
    pagein: InSocket<Html>,
    #[schema_note(humanname="Same page (for chaining)")]
    pageout: OutSocket<Html>,
    #[schema_note(humanname="Extracted URLs")]
    values: OutSocket<Url>,
}


impl Node for UrlInput {
    fn run_script(&mut self, ctx: &SocketIndex, _out: &Arc<ws::Sender>)
    -> Option<JoinHandle<ServerMessage>> {
        let Str(maybe_str) = &self.strent.t;
        let user_string = match maybe_str {
            Some(s) => s.clone(),
            None => return None,
        };
        println!("RUNNING SCRIPT FOR UrlInput: {}", user_string);
        let id = match self.strout.to {
            Some(id) => id,
            None => {return None;},
        };
        let dest = match ctx.get(&id) {
            Some(tx) => tx.clone(),
            None => {return None;},
        };
        Some(thread::spawn(move || {
            dest.send(user_string).unwrap();
            ServerMessage::Done
        }))
    }
}
impl Node for PdfRender {
    fn run_script(&mut self, ctx: &SocketIndex, _out: &Arc<ws::Sender>)
    -> Option<JoinHandle<ServerMessage>> {
        // find out where the output's going:
        let id = match self.pdfout.to {
            Some(id) => id,
            None => {return None;},
        };
        let dest = match ctx.get(&id) {
            Some(tx) => tx.clone(),
            None => {return None;},
        };
        // and where it's coming from:
        let src = self.urlin.piperx.take().unwrap();
        // kick off script:
        println!("RUNNING SCRIPT for pdfrender");
        Some(thread::spawn(move || {
            loop {
                let urlstr = match src.recv() {
                    Ok(t) => t.to_string(),
                    Err(_) => break,
                };
                let urlobj = match url::Url::parse(&urlstr) {
                    Ok(urlobj) => urlobj,
                    Err(error) => {
                        println!("BAD URL: {}", error);
                        continue;
                    }
                };
                let mut pdf = match urlobj.host_str() {
                    Some(host) => host.to_string(),
                    None => {
                        println!("BAD HOST URL: {}", urlstr);
                        continue;
                    },
                };
                for part in urlobj.path_segments().unwrap() {
                    pdf.push('_');
                    pdf.push_str(part);
                }
                pdf.push_str(".pdf");
                pdf.insert_str(0, "working/");
                let mut printarg = "--print-to-pdf=".to_string();
                printarg.push_str(&pdf);
                Command::new("chromium")
                    .arg("--headless").arg("--disable-gpu")
                    .arg(printarg)
                    .arg(urlstr)
                    .status()
                    .expect("Chromium failed us!"); //TODO: @user complaining.
                dest.send(pdf).unwrap();
            }
            ServerMessage::Done
        }))
    }
}

impl Node for PdfDownload {
    fn run_script(&mut self, _ctx: &SocketIndex, out: &Arc<ws::Sender>)
    -> Option<JoinHandle<ServerMessage>> {
        println!("RUNNING SCRIPT FOR pdfdownload");
        // Get input:
        let src = self.pdfin.piperx.take().unwrap();
        let out = Arc::clone(out);
        Some(thread::spawn(move || {
            loop {
                let pdfpath = match src.recv() {
                    Ok(t) => t.to_string(),
                    Err(_) => break,
                };
                out.send(serde_json::to_string(
                        &ServerMessage::Download(pdfpath)).unwrap())
                    .expect("Failed to send client update!");
            }
            ServerMessage::Done
        }))
    }
}
impl Node for Starter {
    fn run_script(&mut self, _ctx: &SocketIndex, _out: &Arc<ws::Sender>)
    -> Option<JoinHandle<ServerMessage>> {
        println!("RUNNING SCRIPT FOR starter");
        None
    }
}

fn get_sender(maybe_id: Option<PortId>, ctx: &SocketIndex) 
    -> Option<Sender<String>> {
    let id = match maybe_id {
        Some(id) => id,
        None => {return None;},
    };
    match ctx.get(&id) {
        Some(tx) => Some(tx.clone()),
        None => None,
    }
}

impl Node for Scraper {
    fn run_script(&mut self, ctx: &SocketIndex, out: &Arc<ws::Sender>)
    -> Option<JoinHandle<ServerMessage>> {
        println!("RUNNING SCRIPT FOR scraper");
        let out = Arc::clone(out);
        // find out where the output's going:
        let pageout = match get_sender(self.pageout.to, ctx) {
            Some(t) => t,
            None => return None
        };
        // and where it's coming from:
        let src = self.urlin.piperx.take().unwrap();
        // kick off script:
        println!("RUNNING SCRIPT for pdfrender");
        Some(thread::spawn(move || {
            let getter = Client::new();
            let mut visited = HashSet::new();
            let mut visited_hashes = HashSet::new();
            let mut hasher = DefaultHasher::new();
            loop {
                let urlstr = match src.recv() {
                    Ok(t) => t.to_string(),
                    Err(_) => break,
                };
                let urlobj = match url::Url::parse(&urlstr) {
                    Ok(t) => t,
                    Err(_) => {
                        out.send(serde_json::to_string(
                                &ServerMessage::Error(
                                format!("Bad URL: {}", urlstr)
                                )
                                ).unwrap()).unwrap();
                        continue
                    }
                };
                if visited.contains(&urlobj) {
                    continue;
                }
                println!("Visited set: {:?}", visited);
                visited.insert(urlobj);
                let mut req = match getter.get(&urlstr).send() {
                    Ok(t) => t,
                    Err(_) => {
                        out.send(serde_json::to_string(
                                &ServerMessage::Error(
                                format!("Bad URL: {}", urlstr)
                                )
                                ).unwrap()).unwrap();
                        continue
                    }
                };
                let page = match req.text() {
                    Ok(t) => t,
                    Err(e) => {
                        out.send(serde_json::to_string(
                                &ServerMessage::Error(
                                format!("Couldn't load: {}, {:?}", urlstr, e)
                                )
                                ).unwrap()).unwrap();
                        continue
                    },
                };
                hasher.write(page.as_bytes());
                if !visited_hashes.insert(hasher.finish()) {
                    continue;
                }
                pageout.send(urlstr).expect("Throwing away base URL!");
                pageout.send(page).expect("Throwing away page output!");
            }
            ServerMessage::Done
        }))
    }
}

impl Node for AttributeExtractor {
    fn run_script(&mut self, ctx: &SocketIndex, _out: &Arc<ws::Sender>)
    -> Option<JoinHandle<ServerMessage>> {
        println!("RUNNING SCRIPT FOR scraper");
        let pageout = get_sender(self.pageout.to, ctx);
        let values = get_sender(self.values.to, ctx);
        if let None = pageout {
            if let None = values {
                return None;
            }
        }
        let tag = match &self.tag.t {
            Str(Some(s)) => s.clone(),
            Str(None) => return None,
        };
        let attribute = match &self.attribute.t {
            Str(Some(s)) => s.clone(),
            Str(None) => return None,
        };
        let external = match &self.external.t {
            Bool(Some(s)) => s.clone(),
            Bool(None) => return None,
        };
        let regextractor = Regex::new(
            &format!(r#"(?m)<{}\s[^>]*?{}="(?P<url>(?:[^"]|\\")*)"#,
            tag, attribute))
            .unwrap();
        let src = self.pagein.piperx.take().unwrap();
        Some(thread::spawn(move || {
            let mut base = None;
            loop {
                let page = match src.recv() {
                    Ok(t) => t.to_string(),
                    Err(_) => break,
                };
                if let None = base {
                    base = Some(page);
                    continue;
                }  else if let Some(baseurl) = base {
                    let baseobj = url::Url::parse(&baseurl).unwrap();
                    for caps in regextractor.captures_iter(&page) {
                        let url = &caps["url"];
                        if !external  {
                            if let Ok(_) = url::Url::parse(&url) {
                                continue; // skip this url
                            }
                        }
                        let urlobj = baseobj.join(url).expect("Malformed URL!"); // TODO: better handling
                        if let Some(tvalues) = &values {
                            tvalues.send(urlobj.into_string()).unwrap();
                        }
                    }
                    if let Some(tpageout) = &pageout {
                        tpageout.send(baseurl).unwrap();
                        tpageout.send(page).unwrap();
                    }
                    base = None
                }
            }
            ServerMessage::Done
        }))
    }
}

wrapper_enum! {
    (Node, NodeWrapper) {Starter, UrlInput, PdfRender, PdfDownload, Scraper,
    AttributeExtractor}
}

#[derive(Debug, Serialize, Deserialize)]
enum ServerMessage {
    Schema(Value),
    Download(String),
    Done, // Indicates a Node has finished
    Error(String), 
    Complete,
}

#[derive(Debug, Deserialize)]
enum ClientMessage {
    Node(NodeWrapper),
    Start(),
}

impl Handler for Server {

    fn on_open(&mut self, _shake: Handshake) -> Result<()> {
        // Let's tell them about all these great node types:
        self.out.send(serde_json::to_string(
                &NodeWrapper::get_schemas()
                ).unwrap()).expect("Sending schemas failed!");
        Ok(())
    }

    fn on_message(&mut self, msg: Message) -> Result<()> {
        if let Message::Text(smsg) = msg {
            let recmsg: ClientMessage = serde_json::from_str(&smsg).unwrap();
            println!("Parsed message to: {:?}", recmsg);
            match recmsg {
                ClientMessage::Node(mut node) => {
                    node.register(self);
                    self.node_vec.push(node);
                },
                ClientMessage::Start() => {
                    println!("Starting! socket lookup table: {:?}", self.socket_index);
                    let mut handles = Vec::new();
                    for node in self.node_vec.drain(..) {
                        let mut n = node;
                        let res = n.run_script(&self.socket_index, &self.out);
                        match res {
                            Some(j) => {
                                handles.push(j);
                            }
                            None => {
                                self.out.send(serde_json::to_string(
                                        &ServerMessage::Done
                                        ).unwrap()).unwrap();
                            }
                        }
                    }
                    // Done with socket index! -- this lets recverrs happen for earlier thread
                    // joins!
                    self.socket_index.clear();
                    // Join everything
                    let out = Arc::clone(&self.out);
                    thread::spawn(move || {
                        for handle in handles.drain(..) {
                            match handle.join() {
                                Ok(msg) => {
                                    out.send(serde_json::to_string(&msg).unwrap())
                                        .unwrap();
                                }
                                Err(_) => {
                                    out.send(serde_json::to_string(
                                            &ServerMessage::Error(
                                            "A node had an internal error!"
                                            .to_string())
                                            ).unwrap())
                                        .unwrap();
                                }
                            }
                        }
                        out.send(serde_json::to_string(
                                &ServerMessage::Complete).unwrap()
                            ).unwrap();
                    });
                },
            }

            Ok(())
        } else {
            Err(Error::new(ws::ErrorKind::Protocol, 
                           std::borrow::Cow::from("Please don't send me binary things")))
        }
    }

    fn on_close(&mut self, code: CloseCode, reason: &str) {
        match code {
            CloseCode::Normal => println!("The client is done with the connection."),
            CloseCode::Away   => println!("The client is leaving the site."),
            _ => println!("The client encountered an error: {}", reason),
        }
    }
}

impl Server {
    fn new(out: ws::Sender) -> Self {
        Server {
            out: Arc::new(out),
            socket_index: HashMap::new(),
            node_vec: Vec::new(),
        }
    }
}

fn main() {
    // working path loc:
    // start webserver for files/downloads/etc
    thread::spawn(|| {
        let router = build_simple_router(|route| {
            route.get("working/*")
                .to_dir(FileOptions::new("working/").build());
            route.get("/").to_file("../frontend/index.html");
            route.get("/patchdata.js").to_file("../frontend/patchdata.js");
            route.get("/patchdata.css").to_file("../frontend/patchdata.css");
        });
        gotham::start("127.0.0.1:4000", router);
    });
    listen("127.0.0.1:4001", |out| Server::new(out, )).unwrap()
}

